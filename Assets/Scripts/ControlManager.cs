﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;


namespace Controller
{
    public class ControlManager : MonoBehaviour
    {

        [DllImport("ControllerInputPlugin")]
        public static extern void initialize(int _numCont);

        [DllImport("ControllerInputPlugin")]
        public static extern int getControllerNumber();

        [DllImport("ControllerInputPlugin")]
        public static extern bool isConnected();

        [DllImport("ControllerInputPlugin")]
        public static extern unsafe int* updateButtonState(int[] result);

        [DllImport("ControllerInputPlugin")]
        public static extern unsafe float* updateTriggerState(float[] result);

        [DllImport("ControllerInputPlugin")]
        public static extern unsafe float* updateStickState(float[] result);


        public Camera mainCam;
        const float move = 0.2f;
        int[] buttons = new int[14];
        float[] triggers = new float[2];
        float[] sticks = new float[4];

        public bool XPress, YPress, APress, BPress = false;
        public bool startPress, backPress = false;
        public bool DPadL, DPadR, DPadD, DPadU = false;
        public bool L1, R1, L2, R2, L3, R3 = false;
        public bool LUp, LDown, LRight, LLeft = false;
        public bool RUp, RDown, RRight, RLeft = false;

        // Use this for initialization
        void Start()
        {

            mainCam = Camera.main;

            initialize(1);

            //Initial check
            Debug.Log("Connected at start: " + isConnected());

            //Player num 0-3
            int i = getControllerNumber();
            Debug.Log("Controller number (-1): " + i);

        }

        // Update is called once per frame
        unsafe void Update()
        {
            updateButtonState(buttons);

            updateTriggerState(triggers);

            updateStickState(sticks);


            //Debug.Log ("Connected: " + isConnected ());


            //Go through each array looking for movement
            for (int i = 0; i < 14; i++)
            {
                switch (buttons[i])
                {
                    case 0:
                        break;
                    case 1: // Dpad Up, Moves camera up towards sky
                        DPadU = true;
                        transform.position += this.transform.up * move;
                        break;
                    case 2: // Dpad Down, Moves camera down towards ground
                        DPadD = true;
                        transform.position += this.transform.up * -move;
                        break;
                    case 3: // DPad Left, Moves camera left (based on direction of camera)
                        DPadL = true;
                        transform.position += this.transform.right * -move;
                        break;
                    case 4: // DPad Right, Moves camera right (based on direction of camera)
                        DPadR = true;
                        transform.position += this.transform.right * move;
                        break;
                    case 5: // Left Thumb, Decreases field of view
                        L3 = true;
                        mainCam.fieldOfView -= move;
                        break;
                    case 6: // Right Thumb, Increases field of view
                        R3 = true;
                        mainCam.fieldOfView += move;
                        break;
                    case 7: // Left Shoulder button, Looks upwards towards sky
                        L1 = true;
                        mainCam.transform.RotateAround(mainCam.transform.position, transform.right, -move * 4);
                        break;
                    case 8: // Right Shoulder button, Looks downward towards ground
                        R1 = true;
                        mainCam.transform.RotateAround(mainCam.transform.position, transform.right, move * 4);
                        break;
                    case 9: // Gamepad A button, Shows top down view
                        APress = true;
                        if (mainCam.transform.position.y != 15.0f)
                        {
                            mainCam.transform.rotation = Quaternion.identity;
                            mainCam.transform.position = new Vector3(0.0f, 15.0f, 0.0f);
                            mainCam.transform.rotation = new Quaternion(1.0f, 0.0f, 0.0f, mainCam.transform.rotation.w);
                        }
                        break;
                    case 10: // Gamepad B Button, Tilts Right
                        BPress = true;
                        mainCam.transform.RotateAround(mainCam.transform.position, transform.forward, -1);
                        break;
                    case 11: // GamePad X Button, Tilts Left
                        XPress = true;
                        mainCam.transform.RotateAround(mainCam.transform.position, transform.forward, 1);
                        break;
                    case 12: // GamePad Y Button, Shows side view
                        YPress = true;
                        if (mainCam.transform.position.x != -15.0f)
                        {
                            mainCam.transform.rotation = Quaternion.identity;
                            mainCam.transform.position = new Vector3(-15.0f, 5.0f, 0.0f);
                            mainCam.transform.rotation = new Quaternion(0.0f, 1.0f, 0.0f, mainCam.transform.rotation.w);
                        }
                        break;
                    case 13: // Back Button/Select Button, Resets camera rotation
                        backPress = true;
                        mainCam.transform.rotation = Quaternion.identity;
                        break;
                    case 14: // Start Button, Resets FOV, Rotation and Camera position
                        startPress = true;
                        mainCam.fieldOfView = 60.0f;
                        mainCam.transform.rotation = Quaternion.identity;
                        mainCam.transform.position = new Vector3(0, 1, -10.0f);
                        break;

                }
            }

            //Trigger pressure
            if (triggers[0] > 0) // Left Trigger, Rotate left on y axis
            {
                L2 = true;
                //Debug.Log("Left Trigger Pressure: " + triggers[0]);
                mainCam.transform.RotateAround(mainCam.transform.position, new Vector3(0, 1, 0), -(triggers[0] / 255.0f));
            }
            if (triggers[1] > 0) // Right Trigger, Rotate right on y axis
            {
                R2 = true;
                //Debug.Log("Right Trigger Pressure: " + triggers[1]);
                mainCam.transform.RotateAround(mainCam.transform.position, new Vector3(0, 1, 0), (triggers[1] / 255.0f));
            }


            //Stick direction
            if (sticks[0] > 2500 || sticks[0] < -2500) // Left Stick X, Moves left and right based on direction of camera
            {
                if (sticks[0] > 2500)
                {
                    LRight = true;
                }
                else if (sticks[0] < -2500)
                {
                    LLeft = true;
                }
                transform.position += this.transform.right * (move * sticks[0]) / 32000.0f;
            }
            if (sticks[1] > 2500 || sticks[1] < -2500) // Left Stick Y, Moves forwards and back based on direction of camera
            {
                if (sticks[1] > 2500)
                {
                    LUp = true;
                }

                else if (sticks[1] < -2500)
                {
                    LDown = true;
                }
                transform.position += this.transform.forward * (move * sticks[1]) / 32000.0f;
            }
            if (sticks[2] > 2580 || sticks[2] < -2580) //Right Stick X, Rotates Left&Right based on direction of camera
            {
                if (sticks[2] > 2500)
                {
                    RRight = true;

                }
                else if (sticks[2] < -2500)
                {
                    RLeft = true;
                }
                mainCam.transform.RotateAround(mainCam.transform.position, transform.up, (sticks[2] / 32000.0f));
            }
            if (sticks[3] > 2580 || sticks[3] < -2580) // Right Stick Y, Rotates Up/Down based on direction of camera
            {
                if (sticks[3] > 2500)
                {
                    RUp = true;
                }
                else if (sticks[3] < -2500)
                {
                    RDown = true;
                }

                mainCam.transform.RotateAround(mainCam.transform.position, transform.right, (sticks[3] / 32000.0f));
            }
        }
    }
}