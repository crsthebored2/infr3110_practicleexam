﻿using UnityEditor;
using UnityEngine;
using Controller;

public class ControllerWindow :EditorWindow {

    public GameObject cam;
    public ControlManager ctrl;
    
   
    public bool X,Y,A,B;
    public bool start,back;
    public bool DPadL, DPadR, DPadD, DPadU;
    public bool L1, L2, L3, R1, R2, R3;
    public bool LUp, LDown, LRight, LLeft;
    public bool RUp, RDown, RRight, RLeft;

    [MenuItem("Window/Controller")]
    public static void ShowWindow()
    {
        GetWindow<ControllerWindow>("Controller Map");
    }
    private Texture2D m_logo = null;
 

    void OnGUI()
    {
        //window code
       // Debug.Log("Window width: " + position.width);
       // Debug.Log("Window height: " + position.height);
        if (m_logo == null)
            m_logo = Resources.Load("ControllerLayout", typeof(Texture2D)) as Texture2D;


        GetComponent();
      
        GUILayout.Label("Contoller Layout");
        GUILayout.Label(m_logo, GUILayout.Width(534), GUILayout.Height(423), GUILayout.MaxWidth(600),GUILayout.MaxHeight(550), GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));



        ButtonCheck();

        //Debug.Log(m_logo);

    }

    void OnInspectorUpdate()
    {
        Repaint();
    }

    void OnEnable()
    { 

    }

    void GetComponent()
    {
        cam = GameObject.FindGameObjectWithTag("MainCamera");
        ctrl = cam.GetComponent<ControlManager>();

        X = ctrl.GetComponent<ControlManager>().XPress;
        Y = ctrl.GetComponent<ControlManager>().YPress;
        A = ctrl.GetComponent<ControlManager>().APress;
        B = ctrl.GetComponent<ControlManager>().BPress;

        start = ctrl.GetComponent<ControlManager>().startPress;
        back = ctrl.GetComponent<ControlManager>().backPress;

        DPadU = ctrl.GetComponent<ControlManager>().DPadU;
        DPadD = ctrl.GetComponent<ControlManager>().DPadD;
        DPadL = ctrl.GetComponent<ControlManager>().DPadL;
        DPadR = ctrl.GetComponent<ControlManager>().DPadR;

        L1 = ctrl.GetComponent<ControlManager>().L1;
        R1 = ctrl.GetComponent<ControlManager>().R1;
        L2 = ctrl.GetComponent<ControlManager>().L2;
        R2 = ctrl.GetComponent<ControlManager>().R2;
        L3 = ctrl.GetComponent<ControlManager>().L3;
        R3 = ctrl.GetComponent<ControlManager>().R3;

        LLeft = ctrl.GetComponent<ControlManager>().LLeft;
        LRight= ctrl.GetComponent<ControlManager>().LRight;
        LUp = ctrl.GetComponent<ControlManager>().LUp;
        LDown= ctrl.GetComponent<ControlManager>().LDown;
        RLeft= ctrl.GetComponent<ControlManager>().RLeft;
        RRight= ctrl.GetComponent<ControlManager>().RRight;
        RUp = ctrl.GetComponent<ControlManager>().RUp;
        RDown = ctrl.GetComponent<ControlManager>().RDown;

    }


    void ButtonCheck()
    {

        if (Y == true)
        {
            m_logo = Resources.Load("ybutton", typeof(Texture2D)) as Texture2D;
            ctrl.YPress = false;
        }

        else if (X == true)
        {
            m_logo = Resources.Load("xbutton", typeof(Texture2D)) as Texture2D;
            // GUILayout.Label(xButton, GUILayout.Width(919), GUILayout.Height(327), GUILayout.MaxWidth(1000), GUILayout.MaxHeight(1000), GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));
            ctrl.XPress = false;

        }

        else if (B == true)
        {
            m_logo = Resources.Load("Bbutton", typeof(Texture2D)) as Texture2D;
            ctrl.BPress = false;
        }


        else if (A == true)
        {
            m_logo = Resources.Load("Abutton", typeof(Texture2D)) as Texture2D;
            ctrl.APress = false;
        }

        else if (start == true)
        {
            m_logo = Resources.Load("start", typeof(Texture2D)) as Texture2D;
            ctrl.startPress = false;
        }

        else if (back == true)
        {
            m_logo = Resources.Load("back", typeof(Texture2D)) as Texture2D;
            ctrl.backPress = false;
        }

        else if (DPadU == true)
        {
            m_logo = Resources.Load("DPadUP", typeof(Texture2D)) as Texture2D;
            ctrl.DPadU = false;
        }

        else if (DPadD == true)
        {
            m_logo = Resources.Load("DPadD", typeof(Texture2D)) as Texture2D;
            ctrl.DPadD = false;
        }

        else if (DPadL == true)
        {
            m_logo = Resources.Load("DPadL", typeof(Texture2D)) as Texture2D;
            ctrl.DPadL = false;
        }

        else if (DPadR == true)
        {
            m_logo = Resources.Load("DPadR", typeof(Texture2D)) as Texture2D;
            ctrl.DPadR = false;
        }

        else if (L1 == true)
        {
            m_logo = Resources.Load("L1", typeof(Texture2D)) as Texture2D;
            ctrl.L1 = false;
        }
        else if (L2 == true)
        {
            m_logo = Resources.Load("L2", typeof(Texture2D)) as Texture2D;
            ctrl.L2 = false;
        }
        else if (L3 == true)
        {
            m_logo = Resources.Load("L3", typeof(Texture2D)) as Texture2D;
            ctrl.L3 = false;
        }
        else if (R1 == true)
        {
            m_logo = Resources.Load("R1", typeof(Texture2D)) as Texture2D;
            ctrl.R1 = false;
        }
        else if (R2 == true)
        {
            m_logo = Resources.Load("R2", typeof(Texture2D)) as Texture2D;
            ctrl.R2 = false;

        }
        else if (R3 == true)
        {
            m_logo = Resources.Load("R3", typeof(Texture2D)) as Texture2D;
            ctrl.R3 = false;

        }


        else if (LLeft == true)
        {
            m_logo = Resources.Load("LLeft", typeof(Texture2D)) as Texture2D;
            ctrl.LLeft = false;

        }
        else if (LRight == true)
        {
            m_logo = Resources.Load("LRight", typeof(Texture2D)) as Texture2D;
            ctrl.LRight = false;

        }
        else if (LUp == true)
        {
            m_logo = Resources.Load("LUp", typeof(Texture2D)) as Texture2D;
            ctrl.LUp = false;

        }
        else if (LDown == true)
        {
            m_logo = Resources.Load("LDown", typeof(Texture2D)) as Texture2D;
            ctrl.LDown = false;

        }
        else if (RLeft == true)
        {
            m_logo = Resources.Load("RLeft", typeof(Texture2D)) as Texture2D;
            ctrl.RLeft = false;

        }
        else if (RRight == true)
        {
            m_logo = Resources.Load("RRight", typeof(Texture2D)) as Texture2D;
            ctrl.RRight = false;

        }
        else if (RUp == true)
        {
            m_logo = Resources.Load("RUp", typeof(Texture2D)) as Texture2D;
            ctrl.RUp = false;

        }
        else if (RDown == true)
        {
            m_logo = Resources.Load("RDown", typeof(Texture2D)) as Texture2D;
            ctrl.RDown = false;

        }




        else
        {
            m_logo = Resources.Load("ControllerLayout2", typeof(Texture2D)) as Texture2D;
        }


    }
}

