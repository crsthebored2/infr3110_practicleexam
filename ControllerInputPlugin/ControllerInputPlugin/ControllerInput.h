#ifndef _XBOX_CONTROLLER_H_
#define _XBOX_CONTROLLER_H_

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <Xinput.h>

//pragma xinput
#pragma comment(lib, "XInput.lib")

// XBOX Controller Class Definition
class ControllerInput
{
private:
	XINPUT_STATE _controllerState;
	int _controllerNum;
	int* buttonInputs;


public:
	ControllerInput(int playerNumber);
	XINPUT_STATE GetState();
	bool IsConnected();
	int getControllerNum();
};

#endif#pragma once
