#include "wrapper.h"
#include <vector>


ControllerInput* player;



void initialize(int _contNum)
{
	//initialize a player with the given controller number
	player = new ControllerInput(_contNum);
}

int getControllerNumber()
{
	//Return controller num
	return player->getControllerNum();
}

bool isConnected()
{
	//returns whether or nothe controller exists
	return player->IsConnected();
}

void updateButtonState(int* result)
{
	//Returns array of buttons pressed, I couldn't figure out how to send dynamic arrays so it currently send all 14 possible buttons
	//and basically assigns them 0's that need to be ignored in the array check in unity when it looks for input

	if (player->IsConnected())
	{
		if (player->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_UP)
			result[0] = 1;
		else 
			result[0] = 0;
		
		if (player->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_DOWN)
			result[1] = 2;
		else
			result[1] = 0;
		
		if (player->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_LEFT)
			result[2] = 3;
		else
			result[2] = 0;

		if (player->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_RIGHT)
			result[3] = 4;
		else
			result[3] = 0;

		if (player->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_THUMB)
			result[4] = 5;
		else
			result[4] = 0;
		
		if (player->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_THUMB)
			result[5] = 6;
		else
			result[5] = 0;
		
		if (player->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_SHOULDER)
			result[6] = 7;
		else
			result[6] = 0;
		
		if (player->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_SHOULDER)
			result[7] = 8;
		else
			result[7] = 0;

		if (player->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_A)
			result[8] = 9;
		else
			result[8] = 0;

		if (player->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_B)
			result[9] = 10;
		else
			result[9] = 0;

		if (player->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_X)
			result[10] = 11;
		else
			result[10] = 0;

		if (player->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_Y)
			result[11] = 12;
		else
			result[11] = 0;

		if (player->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_BACK)
			result[12] = 13;
		else
			result[12] = 0;

		if (player->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_START)
			result[13] = 14;
		else
			result[13] = 0;
	}
	else
	{
		//If null, throw an error in unity
		//this should have one 
	}

}

void updateTriggerState(float* result)
{
	//sends back a two element array with the values for the two trigger pressures
	if (player->IsConnected())
	{
		if (player->GetState().Gamepad.bLeftTrigger > 0)
		{
			result[0] = player->GetState().Gamepad.bLeftTrigger;
		}
		else
		{
			result[0] = 0.f;
		}
		if (player->GetState().Gamepad.bRightTrigger > 0)
		{
			result[1] = player->GetState().Gamepad.bRightTrigger;
		}
		else
		{
			result[1] = 0.f;
		}
	}
	else
	{
		//If null, throw an error in unity
		//this should have one 
	}
}

void updateStickState(float* result)
{
	//sends back a simple 4 element array that has the pressure of the four axis of the stick, or 0 if they aren't pressed (dead zone redundancy, which is currently done in unity)
	if (player->IsConnected())
	{
		if (player->GetState().Gamepad.sThumbLX != 0)
		{
			result[0] = player->GetState().Gamepad.sThumbLX;
		}
		else
		{
			result[0] = 0.f;
		}
		if (player->GetState().Gamepad.sThumbLY != 0)
		{
			result[1] = player->GetState().Gamepad.sThumbLY;
		}
		else
		{
			result[1] = 0.f;
		}
		if (player->GetState().Gamepad.sThumbRX != 0)
		{
			result[2] = player->GetState().Gamepad.sThumbRX;
		}
		else
		{
			result[2] = 0.f;
		}
		if (player->GetState().Gamepad.sThumbRY != 0)
		{
			result[3] = player->GetState().Gamepad.sThumbRY;
		}
		else
		{
			result[4] = 0.f;
		}
	}
	else
	{
		//If null, throw an error in unity
		//this should have one 
	}
}