#include "ControllerInput.h"


ControllerInput::ControllerInput(int playerNumber)
{
	// Set the Controller Number
	_controllerNum = playerNumber - 1;
}

XINPUT_STATE ControllerInput::GetState()
{
	// Clear the memory that controller state is currently using for fresh input
	ZeroMemory(&_controllerState, sizeof(XINPUT_STATE));

	// Get the state using Xinput
	XInputGetState(_controllerNum, &_controllerState);

	return _controllerState;
}

bool ControllerInput::IsConnected()
{
	// Clear the memory that controller state is currently using for fresh input
	ZeroMemory(&_controllerState, sizeof(XINPUT_STATE));

	// Get the state using Xinput
	DWORD Result = XInputGetState(_controllerNum, &_controllerState);

	if (Result == ERROR_SUCCESS)
	{
		return true;
	}
	else
	{
		return false;
	}
}

int ControllerInput::getControllerNum()
{
	return _controllerNum;
}