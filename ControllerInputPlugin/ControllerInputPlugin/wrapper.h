#pragma once

#include "ControllerInput.h"
#include <string>

extern "C"
{
	__declspec(dllexport) void initialize(int _contNum);
	__declspec(dllexport) int getControllerNumber();
	__declspec(dllexport) bool isConnected();

	__declspec(dllexport) void updateButtonState(int* result);
	__declspec(dllexport) void updateTriggerState(float* result);
	__declspec(dllexport) void updateStickState(float* result);
}